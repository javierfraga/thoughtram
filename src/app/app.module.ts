import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ZippyAppComponent} from './zippy-app.component';
import {ZippyComponent} from './my-zippy.component';
import {Tabs} from './tabs/tabs.component';
import {Tab} from './tabs/tab/tab.component';


@NgModule({
  declarations: [
    ZippyAppComponent,
    ZippyComponent,
    Tabs,
    Tab,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [ZippyAppComponent]
})
export class AppModule { }
