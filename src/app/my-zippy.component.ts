import {Component,Input} from '@angular/core';

@Component({
	// moduleId:module.id,
	selector:'my-zippy',
	templateUrl:'my-zippy.component.html',
})

export class ZippyComponent{
	@Input('zippyTitle') title;
	private visible:boolean = true;
	toggle() {
		this.visible = !this.visible;
	}
}
