import {Component} from '@angular/core';

@Component({
	// moduleId:module.id,
	selector:'zippy-app',
	template:`
		<my-zippy title="Details">
			<p>Here is more more details</p>
		</my-zippy>
	`,
})

export class ZippyAppComponent {

}