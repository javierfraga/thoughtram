import {Component,Input} from '@angular/core';
import {Tabs} from '../tabs.component';


@Component({
	selector:'tab',
	template:`
		<div [hidden]="!active">
			<ng-content></ng-content>
		</div>	
	`
})

export class Tab {
	@Input() tabTitle;

	/*
	 * From HTML passes in string assignment to tabTitle
	 * component dep injects parent component and has access to its properties
	 * parent component has an array of this, its child, components
	 * we push every instance of this component to the array in parent component
	 * parent component is able to access the array of child components like a json array
	 */
	constructor(tabs:Tabs) {
		tabs.addTab(this)
	}
}	
