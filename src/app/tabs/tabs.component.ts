import {Component} from '@angular/core';

@Component({
	selector:'tabs',
	// template:`
	// 	<ul>
	// 		<li>Tab 1</li>
	// 		<li>Tab 2</li>
	// 	</ul>
	// 	<ng-content></ng-content>
	// `,
	template:`
		<h3>Click Me</h3>
		<ul>
			<li *ngFor="let tab of tabs" (click)="selectTab(tab)">
				{{tab.tabTitle}}
			</li>
		</ul>
		<ng-content></ng-content>
	`,
})

export class Tabs {
	tabs: Tab[] = [];

	addTab(tab:Tab) {
		// if ( this.tabs.length === 0 ) {
		// 	tab.active = true;
		// }
		this.tabs.push(tab);
	}

	selectTab(tab:Tab) {
		this.tabs.forEach( (tab)=>{
			tab.active = false;
		});
		tab.active = true;
	}

}